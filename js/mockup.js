
$(document).ready( function () {
    $('.input-group.date').datepicker({
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        language: "fr",
        todayHighlight: false,
    });
});
