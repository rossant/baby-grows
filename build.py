
import os
import os.path as op
import re
import sys


def convert_markdown(fn):
    fn_html = fn + '.html'
    os.system('pandoc %s -o %s' % (fn, fn_html))
    return fn_html


def read(fn):
    with open(fn, 'r') as f:
        return f.read()


def escape(text):
    # text = (text).decode('utf-8')

    from html.entities import codepoint2name
    d = dict((chr(code), u'&%s;' % name)
             for code, name in codepoint2name.items()
             if code not in (34, 38, 60, 62))  # exclude "&"
    if u"&" in text:
        text = text.replace(u"&", u"&amp;")
    for key, value in d.items():
        if key in text:
            text = text.replace(key, value)
    return text


regexes = {
    'include-md': re.compile(r'\{% include-md ([^ ]+) %\}'),
}


filename = 'diary-00.html'
filename_built = op.splitext(filename)[0] + '.b.html'
html = read(filename)

with open(filename_built, 'w+') as f:
    r = re.search(regexes['include-md'], html)
    if r:
        fn = r.group(1)
        fn_html = convert_markdown(fn)
        contents = read(fn_html)
        contents = escape(contents)
        html = re.sub(regexes['include-md'], contents, html)
        f.write(html)
